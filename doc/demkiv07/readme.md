# 7. Об'єктно-орієнтована декомпозиція
## Тема
- Використання об'єктно-орієнтованого підходу для розробки об'єкта предметної (прикладної) галузі.

## 1. Вимоги
- Використовуючи об'єктно-орієнтований аналіз, реалізувати класи для представлення сутностей відповідно прикладної задачі - domain-об'єктів.

- Забезпечити та продемонструвати коректне введення та відображення кирилиці.

- Продемонструвати можливість управління масивом domain-об'єктів.
### 1.1 Розробник
- Демків Семен
- КН-921в
- 3 варіант

### 1.2 Загальне завдання
- Розробити прогрму 
- Оформити роботу


### 1.3 Задача
див. у 1

## 2. Вивід у консоль
~~~bash
Демків Семен Семенович
25.11.22
380506720073
Дзиндри 1а
12:25
~~~
  
### 2.1 Засоби ООП
- Java code convention
- JDK:
- Ітератор
### 2.2 Ієрархія та структура класів
- 1. Main
- 2. book

### 2.3 Важливі фрагменти програми:
- Занесення у класс інформацію
~~~java
    public void setName(final String value) {
        this.name = value;
    }

    public void setDate(final String value) {
    	this.date = value;
    }
    public void setTelnumber(final String value) {
    	this.telnumber = value;
    }
    public void setAdress(final String value) {
    	this.adress = value;
    }
    public void setTime(final String value) {
    	this.time = value;
    }
~~~
- Отримання інформації
~~~java
     public String getName() {
        return name;
    }
    public String getDate() {
        return date;
    }
    public String getTelnumber() {
    	return telnumber;
    }
    public String getAdress() {
    	return adress;
    }
    public String getTime() {
        return time;
    }
~~~
## Варіанти використання
Демонстрація роботи ООП 
## Висновки
На цій лабораторній роботі навчились працювати з ооп	