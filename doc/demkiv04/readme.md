# 4. Інтерактивні консольні програми для платформи Java SE∗
## Тема
- Реалізація діалогового режиму роботи з користувачем в консольних програмах мовою Java. 
## 1. Вимоги
- Використовуючи програму рішення завдання лабораторної роботи №3, відповідно до прикладної задачі забезпечити обробку команд користувача у вигляді текстового меню:

  - введення даних;
  - перегляд даних;
  - виконання обчислень;
  - відображення результату;
  - завершення програми і т.д.
- Забезпечити обробку параметрів командного рядка для визначення режиму роботи програми:
  - параметр "-h" чи "-help": відображається інформація про автора програми, призначення (індивідуальне завдання), детальний опис режимів роботи (пунктів меню та параметрів командного рядка);
  - параметр "-d" чи "-debug": в процесі роботи програми відображаються додаткові дані, що полегшують налагодження та перевірку працездатності програми: діагностичні повідомлення, проміжні значення змінних, значення тимчасових змінних та ін.

### 1.1 Розробник
- Демків Семен
- КН-921в
- 3 варіант

### 1.2 Загальне завдання
- Розробити програму 
- Оформити роботу


### 1.3 Задача

- Використовуючи програму рішення завдання лабораторної роботи №3, відповідно до прикладної задачі забезпечити обробку команд користувача у вигляді текстового меню


## 2. Опис програми
- Структура программи:
  - Головний клас з точкою входа в програму знаходиться у пакеті: ua.khpi.oop.borusov04.Main
  -  Графичний інтерфейс знаходится у классі Interface;
  -  Робота з текстом реалізована у классі Textfunction;
- Компіляція програми у терміналі:
~~~bash

Words changing
[Enter a number to choose an action]
1 - Set values
2 - Display values
3 - Run program
4 - Exit

-h
Author: Demkiv Semen

1:
Setting a value for the program

2:
Show entered values

3:
We start the program

4:
Exit program

Task: Використовуючи програму рішення завдання лабораторної роботи №3, відповідно до прикладної задачі забезпечити обробку команд користувача у вигляді текстового меню:

введення даних;
перегляд даних;
виконання обчислень;
відображення результату;
завершення програми і т.д.
Забезпечити обробку параметрів командного рядка для визначення режиму роботи програми:

параметр "-h" чи "-help": відображається інформація про автора програми, призначення (індивідуальне завдання), детальний опис режимів роботи (пунктів меню та параметрів командного рядка);
параметр "-d" чи "-debug": в процесі роботи програми відображаються додаткові дані, що полегшують налагодження та перевірку працездатності програми: діагностичні повідомлення, проміжні значення змінних, значення тимчасових змінних та ін.


Words changing
[Enter a number to choose an action]
1 - Set values
2 - Display values
3 - Run program
4 - Exit

1
First line: 
Hello World
Second line: 
My name is Semen

Words changing
[Enter a number to choose an action]
1 - Set values
2 - Display values
3 - Run program
4 - Exit

2
Hello World
My name is Semen

Words changing
[Enter a number to choose an action]
1 - Set values
2 - Display values
3 - Run program
4 - Exit

3
First group is:
My name is Semen length= 16

Second group is:
Hello World length= 11


Words changing
[Enter a number to choose an action]
1 - Set values
2 - Display values
3 - Run program
4 - Exit

-d
First group is:
My name is Semen length= 16

Second group is:
Hello World length= 11


Words changing
[Enter a number to choose an action]
1 - Set values
2 - Display values
3 - Run program
4 - Exit

4
~~~
  
### 2.1 Засоби ООП
- Java code convention
- JDK:
- StringBuilder
- OOP
### 2.2 Ієрархія та структура класів
- 1. Main
- 2. uiInterface
- 3. replaceText
### 2.3 Важливі фрагменти програми:
- Сортировка по групам
~~~java
static void sortText (String textOne, String textTwo) {
			midlane = (textOne.length()+textTwo.length())/2;
			System.out.println("First group is:");
			if (textOne.length() >= midlane) {
				System.out.println(textOne+" "+"length= "+textOne.length()+"\n");
			}
			if (textTwo.length() >= midlane) {
				System.out.println(textTwo+" "+"length= "+textTwo.length()+"\n");
			}
			System.out.println("Second group is:");
			if (textOne.length() < midlane) {
				System.out.println(textOne+" "+"length= "+textOne.length()+"\n");
			}
			if (textTwo.length() < midlane) {
				System.out.println(textTwo+" "+"length= "+textTwo.length()+"\n");
			}
		}
~~~
## Варіанти використання
Зручне використання функції з допомогою розумного інтерфейса
## Висновки
На цій лабораторній роботі навчились працювати з основами ООП