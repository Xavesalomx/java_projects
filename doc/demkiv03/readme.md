# 3. Утилітарні класи. Обробка масивів і рядків

## Тема
- Розробка власних утилітарних класів.
- Набуття навичок вирішення прикладних задач з використанням масивів і рядків.
## 1. Вимоги
- Розробити та продемонструвати консольну програму мовою Java в середовищі Eclipse для вирішення прикладної задачі за номером, що відповідає збільшеному на одиницю залишку від ділення на 15 зменшеного на одиницю номера студента в журналі групи.
- При вирішенні прикладних задач використовувати латинку.
- Продемонструвати використання об'єктів класу StringBuilder або StringBuffer.
- Застосувати функціональну (процедурну) декомпозицію - розробити власні утилітарні класи (особливий випадок допоміжного класу, див. Helper Class) та для обробки даних використовувати відповідні статичні методи.
- Забороняється використовувати засоби обробки регулярних виразів: класи пакету java.util.regex (Pattern, Matcher та ін.), а також відповідні методи класу String (matches, replace, replaceFirst, replaceAll, split).

### 1.1 Розробник
- Демків Семен
- КН-921в
- 3 варіант

### 1.2 Загальне завдання
- Скомпілювати проект у терміналі
- Оформити роботу
- Продемонструвати використання об'єктів класу StringBuilder або StringBuffer.


### 1.3 Задача
- Ввести декілька рядків. Розбити на дві групи: рядки, довжина яких менша за середню; рядки, довжина яких не менше середньої. Вивести рядки та їх довжину по групах



## 2. Опис програми
- Структура программи:
  - Головний клас з точкою входа в програму знаходиться у пакеті: ua.khpi.oop.demkiv03.Main
- Компіляція програми у терміналі:
  - ![image](assets/console.jpg)
  
### 2.1 Засоби ООП
- Java code convention
- JDK:
- StringBuilder
  
### 2.2 Ієрархія та структура класів
- Програма має лише один клас
### 2.3 Важливі фрагменти програми:
- Знаходження середнього значення довжини сторінок та вивід з угрупованням
~~~java
/**
     * Start function for regroup our strings
     *
     * @param textOne, textTwo
     */
	static void sortText (String textOne, String textTwo) {
		midlane = (textOne.length()+textTwo.length())/2;
		System.out.println("First group is:");
		if (textOne.length() >= midlane) {
			System.out.println(textOne+" "+"length= "+textOne.length()+"\n");
		}
		if (textTwo.length() >= midlane) {
			System.out.println(textTwo+" "+"length= "+textTwo.length()+"\n");
		}
		System.out.println("Second group is:");
		if (textOne.length() < midlane) {
			System.out.println(textOne+" "+"length= "+textOne.length()+"\n");
		}
		if (textTwo.length() < midlane) {
			System.out.println(textTwo+" "+"length= "+textTwo.length()+"\n");
		}
	}
~~~
## Варіанти використання
Сортировка бітів для хешування
## Висновки
На цій лабораторній роботі навчились працювати з StringBuilder
