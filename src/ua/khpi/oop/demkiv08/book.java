package ua.khpi.oop.demkiv08;

import java.util.List;

public class book implements java.io.Serializable {
	
    
    private String name = null;
    private String date = null;
    private String telnumber = null;
    private String adress = null;
    private String time = null;
    
    public String getName() {
        return name;
    }
    public String getDate() {
        return date;
    }
    public String getTelnumber() {
    	return telnumber;
    }
    public String getAdress() {
    	return adress;
    }
    public String getTime() {
        return time;
    }

    
    public void setName(final String value) {
        this.name = value;
    }

    public void setDate(final String value) {
    	this.date = value;
    }
    public void setTelnumber(final String value) {
    	this.telnumber = value;
    }
    public void setAdress(final String value) {
    	this.adress = value;
    }
    public void setTime(final String value) {
    	this.time = value;
    }
    
}

