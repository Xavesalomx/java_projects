package ua.khpi.oop.demkiv08;

import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	private static Scanner scanner;
	
	public static void main(final String[] arguments) {
        final book book = new book();
        
        
        book.setName("Демків Семен Семенович");
        book.setDate("25.11.22");
        book.setTelnumber("380506720073");
        book.setAdress("Дзиндри 1а");
        book.setTime("12:25");
        
        final book book2 = new book();
        
        book2.setName("Володимир Сергійович Лончак");
        book2.setDate("24.10.22");
        book2.setTelnumber("380506567878");
        book2.setAdress("Шевченка 21б");
        book2.setTime("09:54");
    
        List<book> list = new ArrayList<>();
        list.add(book);
        list.add(book2);
        
        listbook full = new listbook();
        full.setBook(list);
        	
        scanner = new Scanner(System.in);
        System.out.print("Entry path folder for file: ");
		String path = scanner.nextLine();

       
        try {
			XMLEncoder x = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(path + ".xml")));
			x.writeObject(full);
			x.close();
			System.out.println("Saving done");
        } catch (FileNotFoundException e) {
		
			e.printStackTrace();
			System.out.println("Error");
		}
	}
	

}
