package ua.khpi.oop.demkiv07;

public class Main {

	public static void main(final String[] arguments) {
        final book book = new book();
        
        
        book.setName("Демків Семен Семенович");
        book.setDate("25.11.22");
        book.setTelnumber("380506720073");
        book.setAdress("Дзиндри 1а");
        book.setTime("12:25");
        
        System.out.print(book.getName() 
        		+ "\n" 
        		+ book.getDate() 
        		+ "\n" 
        		+ book.getTelnumber()
        		+ "\n" 
        		+ book.getAdress()
        		+ "\n" 
        		+ book.getTime());
    }

}
