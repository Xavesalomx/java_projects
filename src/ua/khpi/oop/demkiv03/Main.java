package ua.khpi.oop.demkiv03;

/**
 * Ввести декілька рядків. Розбити на дві групи: рядки, довжина яких менша за середню; рядки, довжина яких не менше середньої. Вивести рядки та їх довжину по групах.
 * @author Demkiv Semen
 * @Version 1.0
 */

import java.util.*; 


public class Main {

	private static Scanner scanner;
	private static String textTwo;
	private static String textOne;
	private static int midlane;
	
	/**
     * Start function for regroup our strings
     *
     * @param textOne, textTwo
     */
	static void sortText (String textOne, String textTwo) {
		midlane = (textOne.length()+textTwo.length())/2;
		System.out.println("First group is:");
		if (textOne.length() >= midlane) {
			System.out.println(textOne+" "+"length= "+textOne.length()+"\n");
		}
		if (textTwo.length() >= midlane) {
			System.out.println(textTwo+" "+"length= "+textTwo.length()+"\n");
		}
		System.out.println("Second group is:");
		if (textOne.length() < midlane) {
			System.out.println(textOne+" "+"length= "+textOne.length()+"\n");
		}
		if (textTwo.length() < midlane) {
			System.out.println(textTwo+" "+"length= "+textTwo.length()+"\n");
		}
	}
 	
    /**
     * Console application entry point
     *
     * @param args command line parameters
     */
    public static void main(String[] args) {

    	scanner = new Scanner(System.in);
		System.out.println("First line: ");
		textOne = scanner.nextLine();
		System.out.println("Second line: ");
		textTwo = scanner.nextLine();
		sortText(textOne, textTwo);
    }

}

